package cree;

import java.io.*;
import java.time.Duration;
import java.util.*;
import java.util.logging.Logger;

import org.apache.commons.cli.Options;

public class CreeSolverCUI {
    public static final Logger logger = Logger.getLogger("mohawk");
    public static String previousCommandFilename = "CreeSolverCUIPreviousCommand.txt";
    public static String previousCmd;

    public static void main(String[] args) {
        CreeSolverInstance inst = new CreeSolverInstance();
        ArrayList<String> argv = new ArrayList<String>();
        ArrayList<String[]> cmds = new ArrayList<String[]>();
        String arg = "";
        Scanner user_input = new Scanner(System.in);

        Options options = new Options();
        inst.setupOptions(options);
        inst.printHelp(options, 120);

        printCommonCommands();

        System.out.println("HELP: To solve all test cases: !run_all");
        System.out.println("HELP: Use '!e' to separate commands, and '!x' to run the previous commands");
        System.out.print("Enter Commandline Argument: ");
        String quotedStr = "";
        StringBuilder fullCommand = new StringBuilder();
        while (true) {
            arg = user_input.next();
            fullCommand.append(arg + " ");

            if (quotedStr.isEmpty() && arg.startsWith("\"")) {
                System.out.println("Starting: " + arg);
                quotedStr = arg.substring(1);
                continue;
            }
            if (!quotedStr.isEmpty() && arg.endsWith("\"")) {
                System.out.println("Ending: " + arg);
                argv.add(quotedStr + " " + arg.substring(0, arg.length() - 1));
                quotedStr = "";
                continue;
            }

            if (!quotedStr.isEmpty()) {
                quotedStr = quotedStr + " " + arg;
                continue;
            }

            // !e indicates that a command is finished and that a new command is wanted
            if (arg.equals("!e")) {
                if (argv.size() > 0) {
                    cmds.add(argv.toArray(new String[1]));
                    argv.clear();
                }
                continue;
            }
            // !x indicates that no other input should be read, and that the commands should be executed
            if (arg.equals("!x")) {
                if (argv.size() > 0) {
                    cmds.add(argv.toArray(new String[1]));
                    argv.clear();
                }
                break;
            }

            if (arg.equals("!p")) {
                Collections.addAll(argv, previousCmd.split(" "));
                continue;
            }

            if (arg.equals("!run_all")) {
                argv.clear();
                cmds = allStats();
                break;
            }

            argv.add(arg);
        }
        user_input.close();

        System.out.println("Commands: " + argv);

        if (!arg.equals("!run_all")) {
            try {
                FileWriter fw;
                fw = new FileWriter(previousCommandFilename, false);
                fw.write(fullCommand.toString().replaceAll("!p", previousCmd));
                fw.close();
            } catch (IOException e) {
                System.out.println("[ERROR] Unable to write out previous command to: " + previousCommandFilename);
            }
        }

        long fullstart = System.currentTimeMillis();
        for (String[] c : cmds) {
            System.out.println("==============================================================");
            System.out.println("== CMD: " + Arrays.toString(c));
            System.out.println("==================START OF OUTPUT=============================");
            long start = System.currentTimeMillis();
            inst = new CreeSolverInstance();
            inst.run(c);
            Duration d = Duration.ofMillis(System.currentTimeMillis() - start);
            System.out.println("====================END OF OUTPUT============================");
            System.out.println("Done [" + humanReadableFormat(d) + "]");
            System.out.println("");
        }

        if (cmds.size() > 1) {
            Duration d = Duration.ofMillis(System.currentTimeMillis() - fullstart);
            System.out.println("\n");
            System.out.println("===========================");
            System.out.println("===========================");
            System.out.println("Done all Tests [" + humanReadableFormat(d) + "]");
        }
    }

    public static String humanReadableFormat(Duration duration) {
        return duration.toString().substring(2).replaceAll("(\\d[HMS])(?!$)", "$1 ").toLowerCase();
    }

    public static ArrayList<String[]> allStats() {
        ArrayList<String[]> cmds = new ArrayList<>();
        String stats = "-run all -loglevel quiet -bulk -absref -mode bmc ";
        // From original Mohawk paper
        cmds.add((stats + "-input data/mohawkT/Mohawk/positive").split(" "));
        cmds.add((stats + "-input data/mohawkT/Mohawk/mixednocr").split(" "));
        cmds.add((stats + "-input data/mohawkT/Mohawk/mixed").split(" "));

        // Rainse Testcases
        cmds.add((stats + "-input data/mohawkT/ranise/testsuiteb/").split(" "));
        cmds.add((stats + "-input data/mohawkT/ranise/testsuitec/hos/").split(" "));
        cmds.add((stats + "-input data/mohawkT/ranise/testsuitec/univ/").split(" "));

        // Uzun Testcases
        cmds.add((stats + "-input data/mohawkT/uzun/roles/").split(" "));
        cmds.add((stats + "-input data/mohawkT/uzun/rules/").split(" "));
        cmds.add((stats + "-input data/mohawkT/uzun/timeslots/").split(" "));

        return cmds;
    }
    public static void printCommonCommands() {
        String allcmd = "-run all -loglevel quiet -bulk -absref -mode bmc ";
        System.out.println("\n\n--- Common Commands ---");
        System.out.println(allcmd + "-input data/mohawkT/regression/reachable/ !x");
        System.out.println(allcmd + "-input data/mohawkT/regression/unreachable/ !x");
        // System.out.println("\n---\n");
        //
        // System.out.println(CreeSolverOptionString.RUN.c("dia") + CreeSolverOptionString.DEBUG.c()
        // + CreeSolverOptionString.SPECFILE.c("data/mohawkT/regression/reachable/reachable-test02.mohawkT")
        // + "!e");
        // System.out.println(CreeSolverOptionString.RUN.c("dia") + CreeSolverOptionString.DEBUG.c()
        // + CreeSolverOptionString.SPECFILE.c("data/mohawkT/regression/reachable/reachable-test07.mohawkT")
        // + "!e");
        // System.out.println(CreeSolverOptionString.RUN.c("dia") + CreeSolverOptionString.DEBUG.c()
        // + CreeSolverOptionString.SPECFILE.c("data/mohawkT/ranise/testsuitec/hos/AGTHos10.mohawkT") + "!e");
        //
        // System.out.println("\n---\n");
        //
        // System.out.println(CreeSolverOptionString.RUN.c("slice") + CreeSolverOptionString.DEBUG.c()
        // + CreeSolverOptionString.SPECFILE.c("data/mohawkT/regression/reachable/reachable-test02.mohawkT")
        // + "!e");
        // System.out.println(CreeSolverOptionString.RUN.c("slice") + CreeSolverOptionString.DEBUG.c()
        // + CreeSolverOptionString.SPECFILE.c("data/mohawkT/regression/reachable/reachable-test07.mohawkT")
        // + "!e");
        // System.out.println(CreeSolverOptionString.RUN.c("slice") + CreeSolverOptionString.DEBUG.c()
        // + CreeSolverOptionString.SPECFILE.c("data/mohawkT/ranise/testsuitec/hos/AGTHos10.mohawkT") + "!e");
        //
        // System.out.println("\n---\n");
        //
        // System.out.println(CreeSolverOptionString.RUN.c("dia") + CreeSolverOptionString.BULK.c()
        // + CreeSolverOptionString.SPECFILE.c("data/mohawkT/regression/reachable/") + "!e");
        // System.out.println(CreeSolverOptionString.RUN.c("dia") + CreeSolverOptionString.BULK.c()
        // + CreeSolverOptionString.SPECFILE.c("data/mohawkT/regression/unreachable/") + "!e");
        // System.out.println(CreeSolverOptionString.RUN.c("dia") + CreeSolverOptionString.BULK.c()
        // + CreeSolverOptionString.SPECFILE.c("data/mohawkT/ranise/testsuiteb/") + "!e");
        // System.out.println(CreeSolverOptionString.RUN.c("dia") + CreeSolverOptionString.BULK.c()
        // + CreeSolverOptionString.SPECFILE.c("data/mohawkT/ranise/testsuitec/hos/") + "!e");
        // System.out.println(CreeSolverOptionString.RUN.c("dia") + CreeSolverOptionString.BULK.c()
        // + CreeSolverOptionString.SPECFILE.c("data/mohawkT/ranise/testsuitec/univ/") + "!e");
        //
        // System.out.println("\n---\n");
        //
        // System.out.println(CreeSolverOptionString.RUN.c("slice") + CreeSolverOptionString.BULK.c()
        // + CreeSolverOptionString.SPECFILE.c("data/mohawkT/regression/reachable/") + "!e");
        // System.out.println(CreeSolverOptionString.RUN.c("slice") + CreeSolverOptionString.BULK.c()
        // + CreeSolverOptionString.SPECFILE.c("data/mohawkT/regression/unreachable/") + "!e");
        // System.out.println(CreeSolverOptionString.RUN.c("slice") + CreeSolverOptionString.BULK.c()
        // + CreeSolverOptionString.SPECFILE.c("data/mohawkT/ranise/testsuiteb/") + "!e");
        // System.out.println(CreeSolverOptionString.RUN.c("slice") + CreeSolverOptionString.BULK.c()
        // + CreeSolverOptionString.SPECFILE.c("data/mohawkT/ranise/testsuitec/hos/") + "!e");
        // System.out.println(CreeSolverOptionString.RUN.c("slice") + CreeSolverOptionString.BULK.c()
        // + CreeSolverOptionString.SPECFILE.c("data/mohawkT/ranise/testsuitec/univ/") + "!e");
        //
        // System.out.println("\n---\n");

        System.out.println("\n###################################################################");
        System.out.println("--- From original Mohawk paper ---");
        System.out.println(allcmd + "-input data/mohawkT/mohawk/positive !x");
        System.out.println(allcmd + "-input data/mohawkT/mohawk/mixednocr !x");
        System.out.println(allcmd + "-input data/mohawkT/mohawk/mixed !x");
        System.out.println("");
        System.out.println(allcmd + "-input data/mohawkT/ranise/testsuiteb !x");
        System.out.println(allcmd + "-input data/mohawkT/ranise/testsuitec/hos !x");
        System.out.println(allcmd + "-input data/mohawkT/ranise/testsuitec/univ !x");
        System.out.println("");
        System.out.println(allcmd + "-input data/mohawkT/uzun/roles !x");
        System.out.println(allcmd + "-input data/mohawkT/uzun/rules !x");
        System.out.println(allcmd + "-input data/mohawkT/uzun/timeslots !x");
        // System.out.println("");
        // System.out.println("------------ OPTIMIZE ME (FIXED) ------------");
        // System.out.println("// Diameter too large when NO Abstraction Refinement");
        // System.out.println(allcmd + "-input data/mohawkT/Mohawk/positive/pos-test05.mohawkT !e");
        //
        // System.out.println("\n------------ BUGS (FIXED) ------------");
        // System.out.println("// Long runtime for BMC mode & abstraction refinement (REDUCE DIAMETER!!!)");
        // System.out.println("// short runtime for BMC mode & NO abstraction refinement");
        // System.out.println("// short runtime for SMC mode & abstraction refinement");
        // System.out.println(CreeSolverOptionString.RUN.c("all") + CreeSolverOptionString.DEBUG.c()
        // + CreeSolverOptionString.MODE.c("bmc") + CreeSolverOptionString.ABSTRACTION_REFINEMENT.c()
        // + CreeSolverOptionString.SPECFILE.c("data/mohawkT/regression/reachable/reachable-test07.mohawkT")
        // + "!e");

        System.out.println("\n-----------------------------------------------");
        try {
            BufferedReader bfr = new BufferedReader(new FileReader(previousCommandFilename));
            previousCmd = bfr.readLine();
            bfr.close();
            System.out.println("Previous Command ('!p' expands to this): " + previousCmd);
        } catch (IOException e) {
            System.out.println("[ERROR] Unable to load previous command!");
        }
    }
}
