package cree;

public enum CreeSolverOptionString {
    /** Displays the help message in the command prompt */
    HELP("help"),
    /** Displays a list of authors who have contributed to this project */
    AUTHORS("authors"),
    /** Displays the current version of this program */
    VERSION("version"),
    /** Checks to see if the default NuSMV programs are installed and run properly,
     * it also displays the version currently installed for each. */
    CHECKNUSMV("checknusmv"),
    /** Flag to turn on or off debug settings. When equal to TRUE, more files are produced:
     * <ul>
     * <li>Each step of Abstraction-Refinement is kept (both Mohawk+T and SMV file)
     * <li>Each step of the Bound Estimation is recorded
     * <li>Each Heuristic is recorded
     * <li><b>This does not change the logging level: console output does not change</b>
     * </ul>
    */
    DEBUG("debug"),
    /** Sets the logging level for the console and the log file */
    LOGLEVEL("loglevel"),
    /** Indicates which file to store the log file in */
    LOGFILE("logfile"),
    /**  */
    NOHEADER("noheader"),
    /** Change the default location to save the results to (saves using CSV format) */
    RESULTSFILE("output"),
    /** Sets the maximum width of the console log. Use 0 for no word-wrapping. */
    MAXW("maxw"),
    /**  */
    LINESTR("linstr"),
    /** The input file/folder to try and solve */
    SPECFILE("input"),
    /**  */
    SMVFILE("smvfile"),
    /**  */
    SPECEXT("ext"),
    /** Treats the {@link CreeSolverOptionString#SPECFILE} as a 
     * folder and uses {@link CreeSolverOptionString#SPECEXT} for the file extension to search for */
    BULK("bulk"),
    /**  */
    MODE("mode"),
    /**  */
    TIMEOUT("timeout"),
    /** Sets the actions that will be performed on the input policies. The following options are available:
     * <ul>
     * <li>"all" -
     * <li>"smv" -
     * <li>"dia" or "diameter" -
     * <li>"slice" -
     * </ul>
    */
    RUN("run"),
    /** The name to attach to the results. Easy way to identify the purpose of a test. */
    RESULT_NAME("name"),
    /**  */
    PROGRAM("program"),
    /**  */
    ABSTRACTION_REFINEMENT("absref"),
    /**  */
    NO_SLICING("noslicing"),
    /** provides an override for the calculated bound to send to the model checker */
    FORCED_BOUND("bound"),
    /**  */
    SKIP_RUNNING("skiprun");

    private String _str;

    private CreeSolverOptionString(String s) {
        _str = s;
    }

    @Override
    public String toString() {
        return _str;
    }

    /** Returns the commandline equivalent with the hyphen and a space following: AUTHORS -> "-authors "
     * 
     * @return */
    public String c() {
        return "-" + _str + " ";
    }

    /** Returns the commandline equivalent with the hyphen and a space following: LOGLEVEL("debug") -> "-loglevel debug
     * "
     * 
     * @param param
     * @return */
    public String c(String param) {
        return "-" + _str + " " + param + " ";
    }
}
