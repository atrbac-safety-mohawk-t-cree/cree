package cree.optimization.slicing;

import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.logging.Logger;

import cree.results.StaticSlicerResult;
import mohawk.global.helper.RoleHelper;
import mohawk.global.pieces.*;
import mohawk.global.timing.MohawkTiming;

public class StaticSlicer {
    public static final Logger logger = Logger.getLogger("mohawk");
    public boolean DEBUG = false;

    /** When TRUE, this will write the policy stored in {@link StaticSlicer#newPolicy} to the filename in
     * {@link StaticSlicer#newPolicyFilename} at the end of the method {@link StaticSlicer#slicePolicy()}. */
    public boolean writeOutNewPolicy = false;

    public MohawkTiming timing;
    public String timerKey;

    public String origPolicyFilename;
    public String newPolicyFilename;
    public MohawkTSize origPolicySize;
    public MohawkTSize newPolicySize;
    public MohawkT origPolicy;
    public MohawkT newPolicy;

    public StaticSlicerResult slicerResult = null;

    // Can pre-fill these to save on processing
    public Set<Role> adminRoles = null;
    public Set<Role> goalRoles = null;

    public ArrayList<Rule> CARules = null;
    public ArrayList<Rule> CRRules = null;
    public ArrayList<Rule> CERules = null;
    public ArrayList<Rule> CDRules = null;

    public Set<Role> CATargetRoles = null;
    public Set<Role> CACRPositivePrecondition = null;

    public Set<Role> CETargetRoles = null;
    public Set<Role> CECDPositivePrecondition = null;

    public StaticSlicer(MohawkT policy, String origPolicyFilename, MohawkTiming timing, String timerKey) {
        origPolicy = policy;
        this.origPolicyFilename = origPolicyFilename;
        origPolicySize = new MohawkTSize(policy);

        slicerResult = new StaticSlicerResult(origPolicyFilename, "");

        newPolicy = new MohawkT("Static Slicer");
        newPolicy.copyMetaData(origPolicy);
        newPolicy.comment = "Created from the Mohawk+T file: " + origPolicyFilename;

        this.timing = timing;
        this.timerKey = timerKey + "-StaticSlice:";
    }

    /** Slices the original policy to the smallest size possible
     * 
     * @throws IOException */
    public void slicePolicy() throws IOException {
        String timerStr = timerKey + "slicePolicy";
        /* TIMING */timing.startTimer(timerStr);

        ////////////////////////////////////////////////////////////////////////////////////
        // Slice01 - Forward Pruning + Contradiction + Contains ALL Goal Roles
        /* TIMING */timing.startTimer(timerStr + "-forwardPruning");

        MohawkT policy01 = forwardPruning(origPolicy);
        slicerResult._slice01Str = new MohawkTSize(policy01).toString();

        /* TIMING */timing.stopTimer(timerStr + "-forwardPruning");
        slicerResult._timingSlice01 = timing.getLastElapsedTime();
        ////////////////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////////////////
        // Slice02 - Backwards Pruning
        /* TIMING */timing.startTimer(timerStr + "-backwardPruning");

        MohawkT policy02 = backwardPruning(policy01);
        slicerResult._slice02Str = new MohawkTSize(policy02).toString();

        /* TIMING */timing.stopTimer(timerStr + "-backwardPruning");
        slicerResult._timingSlice02 = timing.getLastElapsedTime();
        ////////////////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////////////////
        // RESULTS
        newPolicy = policy02;
        newPolicySize = new MohawkTSize(newPolicy);

        slicerResult.addSizes(origPolicySize, newPolicySize);
        ////////////////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////////////////
        // WRITE OUT SLICED POLICY FILE
        if (writeOutNewPolicy) {
            /* TIMING */timing.startTimer(timerStr + "-writeOutNewPolicy");
            FileWriter fw = new FileWriter(newPolicyFilename);
            try {
                fw.write(newPolicy.getString("\n\n", true, true));
                /* TIMING */timing.stopTimer(timerStr + "-writeOutNewPolicy");
            } catch (IOException e) {
                logger.warning("Unable to write the new policy out to the file: " + newPolicyFilename);
                /* TIMING */timing.cancelTimer(timerStr + "-writeOutNewPolicy");
            } finally {
                fw.close();
            }
        }
        ////////////////////////////////////////////////////////////////////////////////////
        /* TIMING */timing.stopTimer(timerStr);
        slicerResult._totalTiming = timing.getLastElapsedTime();
    }

    public MohawkT backwardPruning(MohawkT policy) {
        MohawkT newP = new MohawkT("Generated with BackwardPruning");

        getGoalRoles();

        // Set of roles to use for selective target role inclusion
        Set<Role> Rpos = new HashSet<>();
        Set<Role> Rneg = new HashSet<>();
        Set<Role> Renb = new HashSet<>();
        Set<Role> Rdis = new HashSet<>();

        // Maps for Role to Set<TimeSlot>
        Map<Role, Set<TimeSlot>> Tpos = new HashMap<Role, Set<TimeSlot>>();
        Map<Role, Set<TimeSlot>> Tneg = new HashMap<Role, Set<TimeSlot>>();
        Map<Role, Set<TimeSlot>> Tenb = new HashMap<Role, Set<TimeSlot>>();
        Map<Role, Set<TimeSlot>> Tdis = new HashMap<Role, Set<TimeSlot>>();

        // Working Stacks
        LinkedList<Role> Spos = new LinkedList<>();
        LinkedList<Role> Sneg = new LinkedList<>();
        LinkedList<Role> Senb = new LinkedList<>();
        LinkedList<Role> Sdis = new LinkedList<>();

        // Initialize Sets, Maps, and Stacks
        Spos.addAll(goalRoles);
        Rpos.addAll(goalRoles);

        for (Role g : goalRoles) {
            Tpos.put(g, new HashSet<TimeSlot>(Arrays.asList(policy.query._timeslot)));
        }

        while (!Spos.isEmpty() || !Sneg.isEmpty() || !Senb.isEmpty() || !Sdis.isEmpty()) {
            /////////////////////////////////////////////////////
            // Work on Spos
            workOnStack(policy.canAssign, Spos, Sneg, Senb, Sdis, Rpos, Rneg, Renb, Rdis, Tpos, Tneg, Tenb, Tdis);
            /////////////////////////////////////////////////////

            /////////////////////////////////////////////////////
            // Work on Sneg
            workOnStack(policy.canRevoke, Spos, Sneg, Senb, Sdis, Rpos, Rneg, Renb, Rdis, Tpos, Tneg, Tenb, Tdis);
            /////////////////////////////////////////////////////

            /////////////////////////////////////////////////////
            // Work on Senb
            workOnStack(policy.canEnable, Spos, Sneg, Senb, Sdis, Rpos, Rneg, Renb, Rdis, Tpos, Tneg, Tenb, Tdis);
            /////////////////////////////////////////////////////

            /////////////////////////////////////////////////////
            // Work on Sdis
            workOnStack(policy.canDisable, Spos, Sneg, Senb, Sdis, Rpos, Rneg, Renb, Rdis, Tpos, Tneg, Tenb, Tdis);
            /////////////////////////////////////////////////////
        }

        // Add All relevant CanAssign rules
        for (Rule r : policy.canAssign.getRules()) {
            if (Rpos.contains(r._role)) {
                if (containsAny(Tpos.get(r._role), r._roleSchedule)) {
                    newP.addRule(r, true);
                }
            }
        }

        // Add All relevant CanRevoke rules
        for (Rule r : policy.canRevoke.getRules()) {
            if (Rneg.contains(r._role)) {
                if (containsAny(Tneg.get(r._role), r._roleSchedule)) {
                    newP.addRule(r, true);
                }
            }
        }

        // Add All relevant CanEnable rules
        for (Rule r : policy.canEnable.getRules()) {
            if (Renb.contains(r._role)) {
                if (containsAny(Tenb.get(r._role), r._roleSchedule)) {
                    newP.addRule(r, true);
                }
            }
        }

        // Add All relevant CanDisable rules
        for (Rule r : policy.canDisable.getRules()) {
            if (Rdis.contains(r._role)) {
                if (containsAny(Tdis.get(r._role), r._roleSchedule)) {
                    newP.addRule(r, true);
                }
            }
        }

        // Add Query
        newP.addQuery(policy.query, true);
        newP.expectedResult = policy.expectedResult;

        return newP;
    }

    private void workOnStack(CanBlock canBlock, //
            LinkedList<Role> Spos, LinkedList<Role> Sneg, LinkedList<Role> Senb, LinkedList<Role> Sdis, //
            Set<Role> Rpos, Set<Role> Rneg, Set<Role> Renb, Set<Role> Rdis, //
            Map<Role, Set<TimeSlot>> Tpos, Map<Role, Set<TimeSlot>> Tneg, Map<Role, Set<TimeSlot>> Tenb,
            Map<Role, Set<TimeSlot>> Tdis) {

        LinkedList<Role> stack = null;
        Map<Role, Set<TimeSlot>> timeslots = null;
        Set<TimeSlot> tmp = null;

        if (canBlock instanceof CanAssign) {
            stack = Spos;
            timeslots = Tpos;
        } else if (canBlock instanceof CanRevoke) {
            stack = Sneg;
            timeslots = Tneg;
        } else if (canBlock instanceof CanEnable) {
            stack = Senb;
            timeslots = Tenb;
        } else if (canBlock instanceof CanDisable) {
            stack = Sdis;
            timeslots = Tdis;
        } else {
            logger.severe("[ERROR] Type of CanBlock Unknown: " + (canBlock.getClass()) + "; " + canBlock);
        }

        while (!stack.isEmpty()) {
            Role sRole = stack.pop();

            // Skip roles that have no rules
            if (!canBlock.getTargetRoleToRules().containsKey(sRole)) {
                continue;
            }

            for (Rule r : canBlock.getTargetRoleToRules().get(sRole)) {
                // Skip rules that don't deal with the required timeslots
                if (!containsAny(timeslots.get(sRole), r._roleSchedule)) {
                    continue;
                }

                // Positive Preconditions
                for (Role p : r.getPositivePreconditionRoles()) {
                    if (canBlock instanceof CanAssign || canBlock instanceof CanRevoke) {
                        if (!Rpos.contains(p)) {
                            Spos.push(p);
                            Rpos.add(p);
                            tmp = Tpos.getOrDefault(p, new HashSet<TimeSlot>());
                            tmp.addAll(r._roleSchedule);
                            Tpos.put(p, tmp);
                        }
                    } else {
                        if (!Renb.contains(p)) {
                            Senb.push(p);
                            Renb.add(p);
                            tmp = Tenb.getOrDefault(p, new HashSet<TimeSlot>());
                            tmp.addAll(r._roleSchedule);
                            Tenb.put(p, tmp);
                        }
                    }
                }

                // Negative Preconditions
                for (Role n : r.getNegativePreconditionRoles()) {
                    if (canBlock instanceof CanAssign || canBlock instanceof CanRevoke) {
                        if (!Rneg.contains(n)) {
                            Sneg.push(n);
                            Rneg.add(n);
                            tmp = Tneg.getOrDefault(n, new HashSet<TimeSlot>());
                            tmp.addAll(r._roleSchedule);
                            Tneg.put(n, tmp);
                        }
                    } else {
                        if (!Rdis.contains(n)) {
                            Sdis.push(n);
                            Rdis.add(n);
                            tmp = Tdis.getOrDefault(n, new HashSet<TimeSlot>());
                            tmp.addAll(r._roleSchedule);
                            Tdis.put(n, tmp);
                        }
                    }
                }

                // Admin Role
                if (!r._adminRole.isAllRoles()) {
                    if (!Rpos.contains(r._adminRole)) {
                        Spos.push(r._adminRole);
                        Rpos.add(r._adminRole);
                    }
                    if (!Renb.contains(r._adminRole)) {
                        Senb.push(r._adminRole);
                        Renb.add(r._adminRole);
                    }

                    ArrayList<TimeSlot> t = r._adminTimeInterval.getTimeSlots();
                    tmp = Tpos.getOrDefault(r._adminRole, new HashSet<TimeSlot>());
                    tmp.addAll(t);
                    Tpos.put(r._adminRole, tmp);
                    tmp = Tenb.getOrDefault(r._adminRole, new HashSet<TimeSlot>());
                    tmp.addAll(t);
                    Tenb.put(r._adminRole, tmp);
                }
            }
        }
    }

    /**
     * Returns TRUE if any items in {@code list} are contained within {@code set}
     * @param set 
     * @param list
     * @return
     */
    private boolean containsAny(Set<TimeSlot> set, ArrayList<TimeSlot> list) {
        for (TimeSlot ts : list) {
            if (set.contains(ts)) { return true; }
        }
        return false;
    }

    public MohawkT forwardPruning(MohawkT policy) {
        MohawkT newP = new MohawkT("Generated with ForwardPruning");

        getGoalRoles();
        getCATargetRoles();
        getCETargetRoles();

        for (Rule r : policy.canAssign.getRules()) {
            if (addCACRRuleFPruning(r)) {
                newP.addRule(r, true);
            }
        }
        for (Rule r : policy.canRevoke.getRules()) {
            if (addCACRRuleFPruning(r)) {
                newP.addRule(r, true);
            }
        }
        for (Rule r : policy.canEnable.getRules()) {
            if (addCECDRuleFPruning(r)) {
                newP.addRule(r, true);
            }
        }
        for (Rule r : policy.canDisable.getRules()) {
            if (addCECDRuleFPruning(r)) {
                newP.addRule(r, true);
            }
        }

        newP.addQuery(policy.query, true);
        newP.expectedResult = policy.expectedResult;

        return newP;
    }

    /**
     * Determines if a CanAssign or CanRevoke rule should be added to a policy following Forward Pruning rules
     * @param r the rule that is being considered
     * @return TRUE if the rule should be added to the new policy
     */
    private boolean addCACRRuleFPruning(Rule r) {
        if (!r._adminRole.isAllRoles()) {
            // Do not add rules where the admin rule is impossible to obtain
            if (!CATargetRoles.contains(r._adminRole)) {
                if (DEBUG) {
                    logger.fine("[addCACRRuleFPruning] Skipping rule because AdminRole notIn CATargetRoles; " + r);
                }

                return false;
            }
            // Do not add rules where it is impossible to enable the admin role
            if (!CETargetRoles.contains(r._adminRole)) {
                if (DEBUG) {
                    logger.fine("[addCACRRuleFPruning] Skipping rule because AdminRole notIn CETargetRoles; " + r);
                }

                return false;
            }
        }

        Set<Role> pos = new HashSet<Role>();
        Set<Role> neg = new HashSet<Role>();
        // Do not add rules where no target user can satisfy the precondition
        for (Role role : r._preconditions) {
            if (role == null || role.isAllRoles()) {
                continue;
            }
            if (role.isPositivePrecondition()) {
                pos.add(role);
                if (!CATargetRoles.contains(role)) {
                    if (DEBUG) {
                        logger.fine(
                                "[addCACRRuleFPruning] Skipping rule because precondition notIn CATargetRoles; " + r);
                    }

                    return false;
                }
            } else {
                neg.add(role);
            }
        }

        // Do not add rule if it contains ALL goal roles as positive preconditions
        if (pos.containsAll(goalRoles)) {
            if (DEBUG) {
                logger.fine("[addCACRRuleFPruning] Skipping rule because precondition contains ALL goal roles: "
                        + goalRoles + "; rule: " + r);
            }

            return false;
        }

        // Do not add rule if it contains a contradiction
        pos.retainAll(neg); // pos \cap neg (the intersection of the two sets)
        if (pos.size() > 0) {
            if (DEBUG) {
                logger.fine("[addCACRRuleFPruning] Skipping rule because precondition contains contradiction; " + r);
            }

            return false;
        }

        return true;
    }

    /**
     * Determines if a CanEnable or CanDisable rule should be added to a policy following Forward Pruning rules
     * @param r the rule that is being considered
     * @return TRUE if the rule should be added to the new policy
     */
    private boolean addCECDRuleFPruning(Rule r) {
        if (!r._adminRole.isAllRoles()) {
            // Do not add rules where the admin rule is impossible to obtain
            if (!CATargetRoles.contains(r._adminRole)) {
                if (DEBUG) {
                    logger.fine("[addCECDRuleFPruning] Skipping rule because AdminRole notIn CATargetRoles; " + r);
                }

                return false;
            }
            // Do not add rules where it is impossible to enable the admin role
            if (!CETargetRoles.contains(r._adminRole)) {
                if (DEBUG) {
                    logger.fine("[addCECDRuleFPruning] Skipping rule because AdminRole notIn CETargetRoles; " + r);
                }

                return false;
            }
        }

        Set<Role> pos = new HashSet<Role>();
        Set<Role> neg = new HashSet<Role>();
        // Do not add rules where no target user can satisfy the precondition
        for (Role role : r._preconditions) {
            if (role == null || role.isAllRoles()) {
                continue;
            }
            if (role.isPositivePrecondition()) {
                pos.add(role);
                if (!CETargetRoles.contains(role)) {
                    if (DEBUG) {
                        logger.fine(
                                "[addCECDRuleFPruning] Skipping rule because precondition notIn CETargetRoles; " + r);
                    }

                    return false;
                }
            } else {
                neg.add(role);
            }
        }

        // Do not add rule if it contains a contradiction
        pos.retainAll(neg); // pos \cap neg (the intersection of the two sets)
        if (pos.size() > 0) {
            if (DEBUG) {
                logger.fine("[addCECDRuleFPruning] Skipping rule because precondition contains contradiction; " + r);
            }

            return false;
        }

        return true;
    }

    /** Gets the admin roles and sets the diameter results */
    private void getAdminRoles() {
        if (adminRoles == null) {
            // adminRoles = RoleHelper.getAdministratorRoles(origPolicy.getAllRules(), false);
            adminRoles = origPolicy.roleHelper.getAdminRoles();
            slicerResult._numOrigAdminRoles = adminRoles.size();
        }
    }

    /** Gets the goal roles and sets the diameter results */
    private void getGoalRoles() {
        if (goalRoles == null) {
            goalRoles = new HashSet<Role>(origPolicy.query._roles);
        }
    }

    /** Gets the CA Target Roles and sets the diameter results */
    private void getCATargetRoles() {
        if (CATargetRoles == null) {
            CATargetRoles = origPolicy.canAssign.getTargetRoleToRules().keySet();
        }
    }

    /** Gets the CA/CR Positive Precondition Roles and sets the diameter results */
    private void getCACRPositivePrecondition() {
        if (CACRPositivePrecondition == null) {
            CACRPositivePrecondition = RoleHelper.getPositivePreconditionRoles(origPolicy.canAssign.getRules(), false);
            CACRPositivePrecondition
                    .addAll(RoleHelper.getPositivePreconditionRoles(origPolicy.canRevoke.getRules(), false));
        }
    }

    /** Gets the CE Target Roles and sets the diameter results */
    private void getCETargetRoles() {
        if (CETargetRoles == null) {
            CETargetRoles = RoleHelper.getTargetRoles(origPolicy.canEnable.getRules(), false);

        }
    }

    /** Gets the CE/CD Positive Precondition Roles and sets the diameter results */
    private void getCECDPositivePrecondition() {
        if (CECDPositivePrecondition == null) {
            CECDPositivePrecondition = RoleHelper.getPositivePreconditionRoles(origPolicy.canEnable.getRules(), false);
            CECDPositivePrecondition
                    .addAll(RoleHelper.getPositivePreconditionRoles(origPolicy.canDisable.getRules(), false));
        }
    }

    public MohawkT getSlicedPolicy() {
        return newPolicy;
    }
}
